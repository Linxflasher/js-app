/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, gamePlaying, previousDiceRoll, winScore, winScoreInput, submitScore;

winScore = 100;

function initNewGame() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gamePlaying = true;
    previousDiceRoll = 0;

    submitScore = document.getElementById("submit-win-score");

    document.getElementById("dice-1").style.display = "none";
    document.getElementById("dice-2").style.display = "none";

    document.getElementById("score-0").textContent = 0;
    document.getElementById("score-1").textContent = 0;
    document.getElementById("current-0").textContent = 0;
    document.getElementById("current-1").textContent = 0;
    document.getElementById("name-0").textContent = "Player 1";
    document.getElementById("name-1").textContent = "Player 2";
    document.querySelector(".player-0-panel").classList.remove("active", "winner");
    document.querySelector(".player-1-panel").classList.remove("active", "winner");
    document.querySelector(".player-0-panel").classList.add("active");
    document.getElementById("current-win-score").textContent = winScore;
}

initNewGame();





const rollDiceBtn = document.querySelector(".btn-roll");
const holdScoreBtn = document.querySelector(".btn-hold");
const newGameBtn = document.querySelector(".btn-new");
const diceDOMOne = document.getElementById("dice-1");
const diceDOMTwo = document.getElementById("dice-2");

// Переключаемся на другого игрока и обнуляем результат
function nextPlayer() {

    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;

    document.getElementById("current-0").textContent = "0";
    document.getElementById("current-1").textContent = "0";

    document.querySelector(".player-0-panel").classList.toggle("active");
    document.querySelector(".player-1-panel").classList.toggle("active");

    diceDOMOne.style.display = "none";
    diceDOMTwo.style.display = "none";
}


// Меняем значения кости
rollDiceBtn.addEventListener("click", function () {

    if (gamePlaying) {
        let diceOne = Math.floor(Math.random() * 6 + 1);
        let diceTwo = Math.floor(Math.random() * 6 + 1);
        // let dice = 6;

        diceDOMOne.style.display = "inline-block";
        diceDOMTwo.style.display = "inline-block";
        diceDOMOne.src = "dice-" + diceOne + ".png";
        diceDOMTwo.src = "dice-" + diceTwo + ".png";

        let dice = diceOne + diceTwo;

        if (dice === 6 && previousDiceRoll === 6) {

            scores[activePlayer] = 0;
            document.getElementById("score-" + activePlayer).textContent = 0;
            nextPlayer();
        }



        if (diceOne !== 1 && diceTwo !== 1) {
            // Прибавляем очки, если не выпадает 1 у какой-либо кости
            roundScore += dice;
            document.querySelector("#current-" + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }

        previousDiceRoll = dice;
    }

});

holdScoreBtn.addEventListener("click", function () {

    if (gamePlaying) {

        previousDiceRoll = 0;

        // Добавляем к глобальному счёту
        scores[activePlayer] += roundScore;

        document.getElementById("score-" + activePlayer).textContent = scores[activePlayer];

        // Если глобальный счёт больше 20, то объявляем игрока победителем

        if (scores[activePlayer] >= winScore) {
            document.getElementById("name-" + activePlayer).textContent = "WINNER!";
            diceDOMOne.style.display = "none";
            diceDOMTwo.style.display = "none";
            document.querySelector(".player-" + activePlayer + "-panel").classList.remove("active");
            document.querySelector(".player-" + activePlayer + "-panel").classList.add("winner");
            gamePlaying = false;
        } else {
            nextPlayer();
        }
    }

});

newGameBtn.addEventListener("click", initNewGame);

submitScore.addEventListener("click", () => {
    var inputScore = document.getElementById("win-score-input").value;
    if (inputScore >= 10) {
        winScore = inputScore;
        document.getElementById("current-win-score").textContent = winScore;
        initNewGame();
    } else {
        document.getElementById("current-win-score").textContent = "Enter positive number (minimum 10)";
    }
});